BEGIN {
  found_line = 0
}
ENVIRON["TMUX_COMMAND"] == $1 && $0 ~ /^[a-z\-]+/ {
  if (found_line == 1) {
    print $0
    next
  }
  found_line = 1
  next
}
/^[a-z\-]+/ {
  if (found_line == 1) {
    nextfile
  }
}
{
  if (found_line == 1) {
    print substr($0, match($0, /[^ ]/))
  }
}
