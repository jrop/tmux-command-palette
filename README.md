# tmux-command-palette

A friendly way to browse (and execute) tmux commands.

![ScreenCast](./screencast.gif)

## Why?

After using programs like Sublime, VSCode, FZF, and Telescope.nvim, I have come
to the belief that the Fuzzy Finder is a great way to build _discoverable_ UIs.
It flowed naturally, then, to build one for tmux. Now I use it as a way to
browse tmux's documentation for the various commands it supports, as well as
for having an interactive way to interface with my tmux sessions.

## Installation

**Requirements**: FZF must be installed.

With TPM:

```
set -g @plugin 'tmux-plugins/tmux-yank'
```

## Usage

To show the command palette, press `<Prefix> <C-p>`.  To override the key-bind,
set this in your `~/.tmux.conf`:

```
set -g @tmux_command_palette_key '<C-z>'
```

## License (MIT)

Copyright (c) 2021 <jrapodaca@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
