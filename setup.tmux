#!/bin/bash
# :vim set ft=bash:

__dirname="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

key=$(tmux show-options -gq @tmux_command_palette_key)
if [ "" = "$key" ]; then
  key="C-p"
fi
tmux bind-key $key run-shell -b "$__dirname/scripts/command_palette_spawner \"$__dirname\" || true"
